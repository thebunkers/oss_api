<?php

require_once 'config/DB_Functions.php';
require 'TagsAndMsgs.php';
$db=new DB_Functions();
$response = array("error" => true);//json response

$missing_input = validatePostParams();
function validatePostParams()
{
    $validate = array(
        'user_id' => array('mandatory' => true),
        // Same like the above example you can create all validation
    );
    $missing_input = array();//list of all missing fields if any
    //validate post parameters
    foreach ($validate as $key => $value) {
        if ($value['mandatory']) {
            if (!isset($_POST[$key]))
                array_push($missing_input, $key);
        }
    }
    return $missing_input;
}

if ($missing_input) { //if missing input has some value then show these fields.
    $response["msg"] = "missing parameters (".implode(", ", $missing_input).")";
}else{

    $parts = explode(',',$_POST['user_id']);
    $deletedUsers=array();
    $remainingUsers=array();
    $notfoundUsers=array();
    foreach ($parts as $part){
        $user=$db->isUserExistedById($part);
        if($user){
            $isPostDeleted = $db->deleteUser($part);
            if ($isPostDeleted) {
                // user failed to store
                $response["error"] = false;
                array_push($deletedUsers, $part);
            } else {
                array_push($remainingUsers, $part);
         
            }
        }else{
            array_push($notfoundUsers, $part);
        }
    }
    
    if(empty($deletedUsers)&&empty($remainingUsers)&&empty($notfoundUsers)){
        $response['msg']="Users deleted successfully";
    }else{
       $response['msg']="";
        if(!empty($deletedUsers)){
            $response['msg']=$response['msg']."Deleted users: ". implode(",", $deletedUsers);
        }
        
        if(!empty($remainingUsers)){
            $response['msg']=$response['msg']."Remaining user: ". implode(",", $remainingUsers);
        }
        
        if(!empty($notfoundUsers)){
            $response['msg']=$response['msg']."Users not found: ". implode(",", $notfoundUsers);
        }
       
    }


}
echo json_encode($response);

?>
