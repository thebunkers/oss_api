<?php
/**
 * Created by PhpStorm.
 * User: ReconAppMagic
 * Date: 25/04/17
 * Time: 5:30 PM
 */

define('MSG_LOGIN_SUCCESS', "Login Successful");
define('MSG_LOGIN_FAIL', "Wrong credentials please try again");
define('MSG_GENERAL_ERROR', "Something went wrong Please try again");
define('MSG_REGISTER_SUCCESS', "Registration successful");
define('MSG_REGISTER_FAIL', "We already have a user with this email please try with other email account");

define('MSG_CREATE_MSG_FAIL', "This user does not exist in our database");
define('MSG_CREATE_MSG_SUCCESS', "Your msg sent successfully");
define('MSG_GET_MSG_SUCCESS', "List of all messages fetched successfully");
define('MSG_GET_MSG_FAIL', "Couldn't fetch messages list form server");

define('MSG_CREATE_POST_FAIL_MOVING', "could not move the file ");
define('MSG_CREATE_POST_FAIL', "couldn't send your post");
define('MSG_CREATE_POST_SUCCESS', "Your post sent successfully");

define('MSG_GET_POST_SUCCESS', "List of all posts fetched successfully");
define('MSG_GET_POST_FAIL', "Couldn't fetch posts list from server");

define('MSG_GET_USER_SUCCESS', "List of all Users fetched successfully");
define('MSG_GET_USER_FAIL', "Couldn't fetch Users list from server");

define('MSG_POST_DELETE_SUCCESS', "Post deleted successfully");
define('MSG_POST_DELETE_FAIL', "Couldn't delete the post");
define('MSG_POST_DELETE_FAIL_NOT_FOUND', "This post does not exist.");

define('MSG_USER_DELETE_FAIL_NOT_FOUND', "This user does not exist.");
define('MSG_USER_DELETE_FAIL', "User couldn't be deleted");
define('MSG_USER_DELETE_SUCCESS', "User deleted successfully");
