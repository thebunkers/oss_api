<?php
/**
 * Created by PhpStorm.
 * User: ReconAppMagic
 * Date: 28/04/17
 * Time: 1:13 AM
 */

require_once 'config/DB_Functions.php';
require 'TagsAndMsgs.php';
$db=new DB_Functions();
$response = array("error" => true);//json response


$usersList = $db->getAllUsers();

if($usersList){
    $response['error']=false;
    $response['msg']=MSG_GET_USER_SUCCESS;
    $response["users"]=$usersList;
}else{
    $response['msg']=MSG_GET_USER_FAIL;
}

echo json_encode($response);