<?php

require_once 'config/DB_Functions.php';
require 'TagsAndMsgs.php';
$db=new DB_Functions();
$response = array("error" => true);//json response

$missing_input = validatePostParams();
function validatePostParams()
{
    $validate = array(
        'post_id' => array('mandatory' => true),
        // Same like the above example you can create all validation
    );
    $missing_input = array();//list of all missing fields if any
    //validate post parameters
    foreach ($validate as $key => $value) {
        if ($value['mandatory']) {
            if (!isset($_POST[$key]))
                array_push($missing_input, $key);
        }
    }
    return $missing_input;
}

if ($missing_input) { //if missing input has some value then show these fields.
    $response["msg"] = "missing parameters (".implode(", ", $missing_input).")";
}else{

    $noOfRow=$db->checkPost($_POST['post_id']);
    if($noOfRow>0){
        $isPostDeleted = $db->deletePost($_POST['post_id']);
        if ($isPostDeleted) {
            // user failed to store
            $response["error"] = false;
            $response["msg"] = MSG_POST_DELETE_SUCCESS;
        } else {
            $response["msg"] = MSG_POST_DELETE_FAIL;
        }
    }else{
        $response["msg"] = MSG_POST_DELETE_FAIL_NOT_FOUND;
    }



}
echo json_encode($response);

?>