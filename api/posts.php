<?php
/**
 * Created by PhpStorm.
 * User: ReconAppMagic
 * Date: 28/04/17
 * Time: 1:13 AM
 */

require_once 'config/DB_Functions.php';
require 'TagsAndMsgs.php';
$db=new DB_Functions();
$response = array("error" => true);//json response


$postList = $db->getAllPost();

if($postList){
    $response['error']=false;
    $response['msg']=MSG_GET_POST_SUCCESS;
    $response["posts"]=$postList;
}else{
    $response['msg']=MSG_GET_POST_FAIL;
}

echo json_encode($response);