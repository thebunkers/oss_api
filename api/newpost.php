<?php

require_once 'config/DB_Functions.php';
require 'TagsAndMsgs.php';


$db = new DB_Functions();
$response = array("error" => true);//json response

$missing_input = validatePostParams();
function validatePostParams()
{
    $validate = array(
        'type' => array('mandatory' => true),
        'created_by' => array('mandatory' => true),
        'title' => array('mandatory' => true),
        'post_text' => array('mandatory' => true),
        // Same like the above example you can create all validation
    );
    $missing_input = array();//list of all missing fields if any
    //validate post parameters
    foreach ($validate as $key => $value) {
        if ($value['mandatory']) {
            if (!isset($_POST[$key]))
                array_push($missing_input, $key);
        }
    }
    return $missing_input;
}

/**
 * @param $type
 * @param $createdBy
 * @param $title
 * @param $postText
 * @param $image
 * @return mixed
 * @internal param $db
 * @internal param $file_path
 * @internal param $response
 */
function createNewPost($db,$type, $createdBy, $title, $postText, $image)
{
	  if (!$db->isUserExistedById($_POST['created_by'])) {//check for user in database
        $response["post"] = MSG_CREATE_MSG_FAIL;
    }else{//create new post

    $new_post = $db->createPost($type, $createdBy, $title, $postText, $image);

    if ($new_post) {// File successfully uploaded
        $response['error'] = false;
        $response['message'] = MSG_CREATE_POST_SUCCESS;
        /* $response['post']['post_id'] = $new_post['post_id'];
        $response['post']['type'] = $new_post['type'];
        $response['post']['created_on'] = $new_post['created_on'];
        $response['post']['created_by'] = $new_post['created_by'];
        $response['post']['title'] = $new_post['title'];
        $response['post']['post_text'] = $new_post['post_text'];
        $response['post']['post_image'] = $new_post['post_image']; */
		 
		$response['error'] = false;
        $response['message'] = MSG_CREATE_POST_SUCCESS;
		 $response['post']=$new_post; 
    } else {
        $response['error'] = true;
        $response['message'] = MSG_CREATE_POST_FAIL;
    }
	}
	
	
    return $response;
}

if ($missing_input) { //if missing input has some value then show these fields.
    $response["msg"] = "missing parameters (" . implode(", ", $missing_input) . ")";
} else {

    if (isset($_FILES['image']['name'])) {

        // Path to move uploaded files
        $target_folder= "uploads/";
        //relative path to upload image
        $target_path = $target_folder . basename($_FILES['image']['name']);
        // final file url that is being uploaded
        $file_relative_url='/' . 'api' . '/' . $target_path;

        try {
            // Throws exception incase file is not being moved
            if (move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
                $response = createNewPost($db,$_POST['type'], $_POST['created_by'], $_POST['title'], $_POST['post_text'], $file_relative_url);

            }else{
                // make error flag true
                $response['error'] = true;
                $response['message'] = MSG_CREATE_POST_FAIL_MOVING;
            }
        } catch (Exception $e) {
            // Exception occurred. Make error flag true
            $response['error'] = true;
            $response['message'] = $e->getMessage();
        }
    } else {
        $response = createNewPost($db,$_POST['type'], $_POST['created_by'], $_POST['title'], $_POST['post_text'], null);
	}
}
echo json_encode($response);




?>