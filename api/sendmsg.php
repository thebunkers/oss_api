<?php

require_once 'config/DB_Functions.php';
require 'TagsAndMsgs.php';
$db=new DB_Functions();
$response = array("error" => true);//json response

$missing_input = validatePostParams();
function validatePostParams()
{
    $validate = array(
        'msg_text' => array('mandatory' => true),
        'user_id' => array('mandatory' => true),
        // Same like the above example you can create all validation
    );
    $missing_input = array();//list of all missing fields if any
    //validate post parameters
    foreach ($validate as $key => $value) {
        if ($value['mandatory']) {
            if (!isset($_POST[$key]))
                array_push($missing_input, $key);
        }
    }
    return $missing_input;
}

if ($missing_input) { //if missing input has some value then show these fields.
    $response["msg"] = "missing parameters (".implode(", ", $missing_input).")";
}else{


    if (!$db->isUserExistedById($_POST['user_id'])) {//check for user in database
        $response["msg"] = MSG_CREATE_MSG_FAIL;
    }else{//create new user
        $message = $db->createMsg($_POST['msg_text'], $_POST['user_id']);
        if ($message) {
            // user stored successfully
            $response["error"] = false;
            $response["msg"] = MSG_CREATE_MSG_SUCCESS;/* 
            $response["message"]["msg_id"] = $message["msg_id"];
            $response["message"]["msg_text"] = $message["msg_text"];
            $response["message"]["sent_time"] = $message["sent_time"];
            $response["message"]["sent_by"] = $message["sent_by"]; */
			$response["message"]=$message;
        } else {
            // user failed to store
            $response["error"] = TRUE;
            $response["msg"] = MSG_GENERAL_ERROR;
        }
    }

}
echo json_encode($response);

?>