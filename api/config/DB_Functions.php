<?php

/**
 * Created by PhpStorm.
 * User: ReconAppMagic
 * Date: 21/04/17
 * Time: 4:48 PM
 */
class DB_Functions
{
    private $conn;

    // constructor
    function __construct() {
        require_once __DIR__.'/DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();

    }

    // destructor
    function __destruct() {

    }

    public function storeUser($_f_name, $_l_name, $_gender, $_dob, $_email, $_pass, $_address, $_university, $_visa_grant_date, $_visa_expiry_date) {
        // prepare and bind
        $stmt = $this->conn->prepare("INSERT INTO users (f_name,l_name,gender,dob,email,pass,address,university,visa_grant_date,visa_expiry_date) 
		VALUES (:firstname,:latname,:gender,:dob,:email,:pass,:add,:uni,:v_date, :v_exp_date)");
        $stmt->bindParam(':firstname', $_f_name);
        $stmt->bindParam(':latname', $_l_name);
        $stmt->bindParam(':gender', $_gender);
        $stmt->bindParam(':dob', $_dob);
        $stmt->bindParam(':email', $_email);
        $stmt->bindParam(':pass', $_pass);
        $stmt->bindParam(':add', $_address);
        $stmt->bindParam(':uni', $_university);
        $stmt->bindParam(':v_date', $_visa_grant_date);
        $stmt->bindParam(':v_exp_date', $_visa_expiry_date);




        $result = $stmt->execute();
        // $stmt->close();

        // check for successful store
        if ($result) {
            $stmt=$this->conn->prepare("SELECT * FROM users WHERE email= :email");
            $stmt->bindParam(':email',$_email);
            $stmt->execute();
            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            //return fetched user
            return $user;

        } else {
            return false;
        }


    }

    public function createMsg($msg_text, $sent_by) {
        //set default timezone to india
        date_default_timezone_set('Asia/Kolkata');

        // prepare and bind
        $stmt = $this->conn->prepare("INSERT INTO messages (msg_text,sent_time,sent_by) VALUES (:msg,:send_time,:sendby)");
        $stmt->bindParam(':msg', $msg_text);
        $stmt->bindParam(':send_time', date('Y-m-d H:i:s'));
        $stmt->bindParam(':sendby', $sent_by);

        $result = $stmt->execute();
        //line for getting last added message
        $LAST_ID =$this->conn->lastInsertId();

        // check for successful store
        if ($result) {
            $stmt=$this->conn->prepare("SELECT * FROM messages WHERE msg_id= :msgid");
            $stmt->bindParam(':msgid',$LAST_ID);
            $stmt->execute();

            $msg=$stmt->fetch(PDO::FETCH_ASSOC);
            $msg['user'] = $this->getUserByID($msg['sent_by']);
            return $msg;

        }
        else {
            return false;
        }

    }

    public function createPost($_type, $_createdBy, $_title, $_postText, $_postImage) {
        //set default timezone to india
        date_default_timezone_set('Asia/Kolkata');
        // prepare and bind
        $stmt = $this->conn->prepare("INSERT INTO posts (type,created_on,created_by,title,post_text,post_image) VALUES (:type, :created_on, :created_by, :title,:post_text,:post_image)");
        if ( false===$stmt ) {
            die('prepare() failed: ' . htmlspecialchars($this->conn->error));
        }
        $rc=$stmt->bindParam(':type', $_type);
        $rc=$stmt->bindParam(':created_on', date('Y-m-d H:i:s'));
        $rc=$stmt->bindParam(':created_by', $_createdBy);
        $rc=$stmt->bindParam(':title', $_title);
        $rc=$stmt->bindParam(':post_text', $_postText);
        $rc=$stmt->bindParam(':post_image', $_postImage);

        if ( false===$rc ) {
            die('bind_param() failed: ' . htmlspecialchars($stmt->error));
        }

        $result = $stmt->execute();
        if ( false===$result ) {
            die('execute() failed: ' . $stmt->error);
        }

        $LAST_ID =$this->conn->lastInsertId();

        // check for successful store
        if ($result) {
            $stmt=$this->conn->prepare("SELECT * FROM posts WHERE post_id= :postid");
            $stmt->bindParam(':postid',$LAST_ID);
            $stmt->execute();
            //    $post=$stmt->get_result()->fetch_assoc();
            $post=$stmt->fetch(PDO::FETCH_ASSOC);
            $post['user'] = $this->getUserByID($post['created_by']);
            $allpost = array();

            return $post;

        } else {
            return false;
        }

    }


    /**
     * Check user is existed or not
     */
    public function isUserExisted($email) {
        $stmt = $this->conn->prepare("SELECT email from users WHERE email = :email");
        $stmt->bindParam(':email', $email);
        $stmt->execute();

        //$stmt->store_result();
        $count = $stmt->rowCount();
        if ( $count > 0) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Check user is existed or not
     */
    public function isUserExistedById($_id) {
        $stmt = $this->conn->prepare("SELECT email from users WHERE user_id = :userid");
        $stmt->bindParam(':userid', $_id);

        $stmt->execute();

        $count = $stmt->rowCount();
        if ( $count > 0) {
            return true;
        } else {
            return false;
        }


    }

    //using pdo
    public function getUserByCredential($_email, $_pass){
        // query
        $result =$this->conn->prepare("SELECT * FROM users WHERE email= :email AND pass= :pass");
        $result->bindParam(':email', $_email);
        $result->bindParam(':pass', $_pass);
        $result->execute();

        if ($result->execute()) {
            $user=$result->fetch(PDO::FETCH_ASSOC);
            return $user;
        }else{
            die($result->errno|$result->error);
        }

    }


    public function getUserByID($_id){
        $stmt=$this->conn->prepare("SELECT * FROM users WHERE users.user_id = :id");
        $stmt->bindParam(':id',$_id);
        $stmt->execute();

        if ($stmt->execute()) {
            $user=$stmt->fetch(PDO::FETCH_ASSOC);
            return $user;
        }else{
            die($stmt->errno|$stmt->error);
        }
    }


    public function getAllMsg(){
        $stmt=$this->conn->prepare("SELECT * FROM messages");
        $stmt->execute();

        $allMsgs = array();
        if($stmt->execute()){
            while($msg = $stmt->fetch(PDO::FETCH_ASSOC)){
                $user = $this->getUserByID($msg['sent_by']);
                $msg['user'] = $user;
                array_push($allMsgs, $msg);
            }
        }else{
            die($stmt->errno|$stmt->error);
        }
        return $allMsgs;
    }


    public function getAllPost(){
        $stmt=$this->conn->prepare("SELECT * FROM posts");
        $stmt->execute();


        if($stmt->execute()){
            $allMsgs = array();
            while($msg = $stmt->fetch(PDO::FETCH_ASSOC)){
                $user = $this->getUserByID($msg['created_by']);
                $msg['user'] = $user;
                array_push($allMsgs, $msg);
            }
        }else{
            die($stmt->errno|$stmt->error);
        }
        return $allMsgs;
    }


    public function getAllUsers(){
        $user='user';
        // $stmt=$this->conn->prepare("SELECT * FROM users WHERE user_type = :type");
        $stmt=$this->conn->prepare("SELECT * FROM users WHERE user_type = :type");
        $stmt->bindParam(':type',$user);
        $stmt->execute();

        $allUsers = array();
        if($stmt->execute()){
            while($user = $stmt->fetch(PDO::FETCH_ASSOC)){
                array_push($allUsers, $user);
            }
        }else{
            die($stmt->errno|$stmt->error);
        }
        return $allUsers;
    }




    public function deletePost($post_id){

        $stmt=$this->conn->prepare("DELETE FROM posts WHERE post_id = :post_id");
        $stmt->bindParam(':post_id', $post_id);
        $stmt->execute();

        $count = $stmt->rowCount();
        if ( $count > 0) {
            return true;
        } else {
            return false;
        }



    }
    public function deleteUser($userid){

        $stmt=$this->conn->prepare("DELETE FROM users WHERE user_id =:userid");
        $stmt->bindParam(':userid', $userid);
        $stmt->execute();

        $count = $stmt->rowCount();
        if ( $count > 0) {
            return true;
        } else {
            return false;
        }


    }

    public function checkPost($postid){
        $stmt=$this->conn->prepare("SELECT * FROM posts WHERE post_id = :postid");
        $stmt->bindParam(':postid',$postid);
        $stmt->execute();

        if ($stmt->execute()) {
            $user=$stmt->fetch(PDO::FETCH_ASSOC);
            return $user;
        } else {
            die($stmt->errno|$stmt->error);
        }

        return 0;
    }
}
