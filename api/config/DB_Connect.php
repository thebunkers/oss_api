<?php

/**
 * A class file to connect to database
 */
class DB_CONNECT {


    // constructor
    function __construct() {
        // connecting to database
        $this->connect();
    }

    // destructor
    function __destruct() {

    }

    /**
     * Function to connect with database
     */
    function connect() {
        // import database connection variables
      // require_once __DIR__ . '/DB_Config.php';

        // Create connection
    //   $conn = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);

        // Check connection
     //   if ($conn->connect_error) {
     //     die("Connection failed: " . $conn->connect_error);
    //   }
	
	   
	   try {
		   
		   require_once __DIR__ . '/DB_Config.php';
           // $db_host = 'mysql.hostinger.in';  //  hostname
        //    $db_name = 'u762252331_ossdb';  //  databasename
         //   $db_user = 'u762252331_admin';  //  username
         //   $user_pw = 'Ossproject123';  //  password

           $conn = new PDO('mysql:host='.DB_SERVER.'; dbname='.DB_DATABASE, DB_USER, DB_PASSWORD);
           $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
           $conn->exec("SET CHARACTER SET utf8");  //  return all sql requests as UTF-8

			
        }
        catch (PDOException $err) {  
            echo "harmless error message if the connection fails".$err->getMessage() . "<br/>";
            
        }
		
        return  $conn;
    }


}

?>