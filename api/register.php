<?php

require_once 'config/DB_Functions.php';
require 'TagsAndMsgs.php';
$db=new DB_Functions();
$response = array("error" => true);//json response

$missing_input = validatePostParams();
function validatePostParams()
{
    $validate = array(
        'f_name' => array('mandatory' => true),
        'l_name' => array('mandatory' => true),
        'gender' => array('mandatory' => true),
        'dob' => array('mandatory' => true),
        'email' => array('mandatory' => true),
        'pass' => array('mandatory' => true),
        'address' => array('mandatory' => true),
        'university' => array('mandatory' => true),
        'visa_grant_date' => array('mandatory' => true),
        'visa_expiry_date' => array('mandatory' => true),
        // Same like the above example you can create all validation
    );
    $missing_input = array();//list of all missing fields if any
    //validate post parameters
    foreach ($validate as $key => $value) {
        if ($value['mandatory']) {
            if (!isset($_POST[$key]))
                array_push($missing_input, $key);
        }
    }
    return $missing_input;
}

if ($missing_input) { //if missing input has some value then show these fields.
    $response["msg"] = "missing parameters (".implode(", ", $missing_input).")";
}else{


    if ($db->isUserExisted($_POST['email'])) {//check for user in database
        $response["msg"] = MSG_REGISTER_FAIL;
    }else{//create new user
        $user = $db->storeUser($_POST['f_name'], $_POST['l_name'], $_POST['gender'], $_POST['dob'], $_POST['email'], $_POST['pass'], $_POST['address'], $_POST['university'], $_POST['visa_grant_date'], $_POST['visa_expiry_date']);
        if ($user) {
            // user stored successfully
            $response["error"] = false;
            $response["msg"] = MSG_REGISTER_SUCCESS;
            $response["user"]["user_id"] = $user["user_id"];
            $response["user"]["user_type"] = $user["user_type"];
            $response["user"]["f_name"] = $user["f_name"];
            $response["user"]["l_name"] = $user["l_name"];
            $response["user"]["gender"] = $user["gender"];
            $response["user"]["dob"] = $user["dob"];
            $response["user"]["email"] = $user["email"];
            $response["user"]["address"] = $user["address"];
            $response["user"]["university"] = $user["university"];
            $response["user"]["visa_grant_date"] = $user["visa_grant_date"];
            $response["user"]["visa_expiry_date"] = $user["visa_expiry_date"];
        } else {
            // user failed to store
            $response["error"] = TRUE;
            $response["msg"] = MSG_GENERAL_ERROR;
        }
    }

}
echo json_encode($response);

?>